require("PGStateMachine")
require("PGSpawnUnits")
local VariantUtility = require("libVariant")

function Definitions()
    ServiceRate = 1
    Define_State("State_Init", State_Init)
end

function State_Init(message)
    if message == OnEnter then
        if Get_Game_Mode() ~= "Space" then
            ScriptExit()
        end
        VariantUtility:enterBattlefield(Object)
		local dummyVar = "Test"
		local shipVariantType = "Layer_Z_Dummy"
		if Object.Get_Type("Nebulon_B_Frigate") then
			shipVariantType = "Nebulon_B_Frigate"
		elseif Object.Get_Type("Marauder_Missile_Cruiser") then
			shipVariantType = "Marauder_Missile_Cruiser"
		end
		local shipVariantTable = {
			["Nebulon_B_Frigate"] = {
				"Marauder_Missile_Cruiser",
				"Alliance_Assault_Frigate",
				"MC30_Frigate",
			},
			["Marauder_Missile_Cruiser"] = {
				"Marauder_Missile_Cruiser",
				"Marauder_Missile_Cruiser",
				"Marauder_Missile_Cruiser",
			},
		}
		
		local shipVariant = shipVariantTable[shipVariantType]
		if shipVariant then
			local shipVariantFinal = shipVariant[GameRandom(1,table.getn(shipVariant))]
			if shipVariantFinal then
				unit = Spawn_Unit(Find_Object_Type(shipVariantFinal), Object.Get_Position(), Object.Get_Owner())[1] -- Spawn_Unit returns a table!
				unit.Cinematic_Hyperspace_In(1)
			else
				dummyVar = "Layer_Z_Dummy"
			end
		else
			dummyVar = "Layer_Z_Dummy"
		end
	elseif message == OnUpdate then
			if not TestValid(unit) then
				Object.Hyperspace_Away()
				ScriptExit()
			end
    end
end