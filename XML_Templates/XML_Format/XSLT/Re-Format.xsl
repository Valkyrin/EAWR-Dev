<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" indent="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="@*|node()">
		
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
		
	</xsl:template>

	<!-- ignore everything that isn't an attribute or node and is empty -->
	<xsl:template match="*[not(@*|node()) and normalize-space() = '']"/>
	
</xsl:stylesheet>